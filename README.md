# demo-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
###create enviroment for project

```
sudo npm install -g @vue/cli
Vue -V
OR 
Vue --version
npm i -g vue-cli yarn

Yarn
<!--Yarn build-->
Yarn serve

```


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
